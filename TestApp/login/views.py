from django.shortcuts import render
from django.http import HttpResponse
from django.http import JsonResponse
from django.template import Context, Template
from django.template import loader
from django.views.decorators.csrf import csrf_exempt
from django.contrib.auth import authenticate
import json
from django.contrib.auth.models import User
from rest_framework.decorators import api_view



@csrf_exempt
def index(request):
    save_name = ''
    template = loader.get_template('login/login.html')
    template_done = loader.get_template('login/login_done.html')

    if request.method == 'POST':
        save_name = request.POST.get("your_name", "")
        passwd = request.POST.get("your_pass", "")

        if authenticate(username=save_name , password=passwd):
            return HttpResponse(template_done.render({'saved_name': save_name, 'message': 'Logowanie pomyślne'}, request)) 
        else:
            return HttpResponse(template.render({'saved_name': save_name, 'message': 'Logowanie nieudane'}, request)) 

    
    return HttpResponse(template.render({'saved_name': save_name }, request)) 


@csrf_exempt
@api_view(['POST'])
def rest_login(request):
    json_data = json.loads(request.body)
    if authenticate(username=json_data['username'] , password=json_data['password']):
        return JsonResponse( {'login':'100'} )
    return JsonResponse( {'login':'200'} )



@csrf_exempt
def add_user(request):
    if request.method == 'POST':
        name = request.POST.get("your_name", "")
        passwd = request.POST.get("your_pass", "")
        user = User.objects.create_user(name, '', passwd)
        user.save()
    template = loader.get_template('login/add_user.html')
    return HttpResponse(template.render({}, request))  
