from django.urls import path

from . import views

urlpatterns = [
    path('login/', views.index, name='login'),
    path('api_login/', views.rest_login, name='rest_login'),
    path('add_user/', views.add_user, name='add_user')
]