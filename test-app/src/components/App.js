import './App.css';
import { BrowserRouter as Router, Route, Switch } from 'react-router-dom';
import login_page from './Login'
import { CookiesProvider } from 'react-cookie';


function App() {
  return (
    <CookiesProvider>
      <Router>
        <Switch>
          <Route path="/" component={login_page} />
        </Switch>
      </Router>
    </CookiesProvider>
  );
}

export default App;
