import React from 'react';
import './Login.css';
import { withCookies, Cookies } from 'react-cookie';
import { instanceOf } from "prop-types";

class Login extends React.Component {

    constructor(props){
        super (props);
        this.state = {
        username: '',
        password: '',
        loading: false 
        }       
    }

    static propTypes = {
        cookies: instanceOf(Cookies).isRequired
    };

    LoginContainer = () => {

        const handleLogin = () => {
    
            const fetchData = async () => {
                await fetch('http://localhost:8000/api_login/', {
                  method: 'POST',
                  headers: {
                    'Content-Type': 'application/json',
                  },
                  body: JSON.stringify({ username: this.state.username, password: this.state.password }),
                })
                  .then((response) => response.json())
                  .then((response) => {
                   if (response['login'] === '100')  this.props.cookies.set("login", true, {path: "/"});
                   else alert("Something is wrong!")
                  })
                  .catch((error) => {
                    console.log(error);
                  });
              };
    
              fetchData();
        }

        const handleKeyPress = (event) => {
            if (event.key === 'Enter') {
                handleLogin();
            }
        }
    
        return (
            <div className="login_container" >
                <span className="login_sign"> Just login! </span>
                <input
                    className="email"
                    type="text"
                    placeholder="Username"
                    onChange = {(event) => this.setState({username:event.target.value})}
                />
                <input
                    className="pass"
                    type="password"
                    placeholder="Password"
                    onChange = {(event) => this.setState({password:event.target.value})}
                    onKeyPress = {handleKeyPress}
                />
                <input
                    className="login_btn"
                    type="button" 
                    value={this.state.loading ? 'Loading...' : 'Login'} 
                    onClick={handleLogin} disabled={this.state.loading} 
                />
            </div>
        )
    }
    
    LogoutContainer = () => {
    
        const handleLogout = () => {
            this.props.cookies.set("login", false, {path: "/"});
        }
    
        return (
            <div className="login_container" >
                <span className="login_sign"> Just logout! </span>
                <input
                    className="logout_btn"
                    type="button" 
                    value="Logout"
                    onClick={handleLogout}
                />
            </div>
        )
    }

    render () {
        const { cookies } = this.props;

        return(
            <div className="background_img" >
                { cookies.cookies['login'] === 'true' ? < this.LogoutContainer /> : < this.LoginContainer /> }
            </div>
        )
    } 
}

export default withCookies( Login );